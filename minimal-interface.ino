#include <Adafruit_NeoPixel.h>

int LED_COUNT = 8;

static int pinN = 6; // NeoPixel pin
static int pinS = 8;

static int redVal = 0;
static int greenVal = 0;
static int blueVal = 0;

// array position is led position
int LED_Array[8][3] = {
  { 0, 0, 0 },
  { 0, 0, 0 },
  { 0, 0, 0 },
  { 0, 0, 0 },
  { 0, 0, 0 },
  { 0, 0, 0 },
  { 0, 0, 0 },
  { 0, 0, 0 }  
};

static int pinA = 2; // Our first hardware interrupt pin is digital pin 2
static int pinB = 3; // Our second hardware interrupt pin is digital pin 3
volatile byte aFlag = 0; // let's us know when we're expecting a rising edge on pinA to signal that the encoder has arrived at a detent
volatile byte bFlag = 0; // let's us know when we're expecting a rising edge on pinB to signal that the encoder has arrived at a detent (opposite direction to when aFlag is set)
volatile byte encoderPos = 0; //this variable stores our current value of encoder position. Change to int or uin16_t instead of byte if you want to record a larger range than 0-255
volatile byte oldEncPos = 0; //stores the last encoder position value so we can compare to the current reading and see if it has changed (so we know when to print to the serial monitor)
volatile byte reading = 0; //somewhere to store the direct values we read from our interrupt pins before checking to see if we have moved a whole detent

int buttonState = 0;
bool pressed = false;
int adjustmentState = 0;
int pixelPos = 0;

Adafruit_NeoPixel strip = Adafruit_NeoPixel(LED_COUNT, pinN, NEO_GRB + NEO_KHZ800);

void setup() {
  pinMode(pinA, INPUT_PULLUP); // set pinA as an input, pulled HIGH to the logic voltage (5V or 3.3V for most cases)
  pinMode(pinB, INPUT_PULLUP); // set pinB as an input, pulled HIGH to the logic voltage (5V or 3.3V for most cases)
  attachInterrupt(0, PinA, RISING); // set an interrupt on PinA, looking for a rising edge signal and executing the "PinA" Interrupt Service Routine (below)
  attachInterrupt(1, PinB, RISING); // set an interrupt on PinB, looking for a rising edge signal and executing the "PinB" Interrupt Service Routine (below)

  pinMode(pinS, INPUT);

  strip.begin();
  strip.show(); // Initialize all pixels to 'off'

  Serial.begin(115200); // start the serial monitor link
  Serial.println("Started.");
  displayReady();
}

void PinA() {
  cli(); //stop interrupts happening before we read pin values
  reading = PIND & 0xC; // read all eight pin values then strip away all but pinA and pinB's values
  if (reading == B00001100 && aFlag) { //check that we have both pins at detent (HIGH) and that we are expecting detent on this pin's rising edge
    encoderPos --; //decrement the encoder's position count
    bFlag = 0; //reset flags for the next turn
    aFlag = 0; //reset flags for the next turn
  }
  else if (reading == B00000100) bFlag = 1; //signal that we're expecting pinB to signal the transition to detent from free rotation
  sei(); //restart interrupts
}

void PinB() {
  cli(); //stop interrupts happening before we read pin values
  reading = PIND & 0xC; //read all eight pin values then strip away all but pinA and pinB's values
  if (reading == B00001100 && bFlag) { //check that we have both pins at detent (HIGH) and that we are expecting detent on this pin's rising edge
    encoderPos ++; //increment the encoder's position count
    bFlag = 0; //reset flags for the next turn
    aFlag = 0; //reset flags for the next turn
  }
  else if (reading == B00001000) aFlag = 1; //signal that we're expecting pinA to signal the transition to detent from free rotation
  sei(); //restart interrupts
}

void loop() {

  if (oldEncPos != encoderPos) {
    
    switch (adjustmentState) {
      case 1:
        Serial.println("Red adjustment");
        LED_Array[pixelPos][0] = encoderPos;
        break;
        
      case 2:
        Serial.println("Green adjustment");
        LED_Array[pixelPos][1] = encoderPos;
        break;
        
      case 3:
        Serial.println("Blue adjustment");
        LED_Array[pixelPos][2] = encoderPos;
        break;
        
      case 4:
        Serial.println("Position adjustment");

        if (encoderPos == 255) {
          Serial.println("bottom");
          encoderPos = 7;
        } else if (encoderPos > 7) {
          Serial.println("top");
          encoderPos = 0;
        }
        
        pixelPos = encoderPos;

        strip.setPixelColor(pixelPos, 75, 75, 75);
        strip.show();
        delay(100);
        strip.setPixelColor(pixelPos, 0, 0, 0);
        strip.show();
        
        break;
        
    }
    
    displayLED();
    
    Serial.println(encoderPos); 
    oldEncPos = encoderPos;
  }

  buttonState = digitalRead(pinS);
  if (buttonState > 0 && !pressed) {
    pressed = true;
    adjustmentState++;
    switch (adjustmentState) {
      case 1:
        Serial.println("Red selection");
        encoderPos = LED_Array[pixelPos][0];

        strip.setPixelColor(pixelPos, 75, 0, 0);
        strip.show();
        delay(50);
        strip.setPixelColor(pixelPos, 0, 0, 0);
        strip.show();
        
        break;
        
      case 2:
        Serial.println("Green selection");
        encoderPos = LED_Array[pixelPos][1];

        strip.setPixelColor(pixelPos, 0, 75, 0);
        strip.show();
        delay(50);
        strip.setPixelColor(pixelPos, 0, 0, 0);
        strip.show();

        break;
        
      case 3:
        Serial.println("Blue selection");
        encoderPos = LED_Array[pixelPos][2];

        strip.setPixelColor(pixelPos, 0, 0, 75);
        strip.show();
        delay(50);
        strip.setPixelColor(pixelPos, 0, 0, 0);
        strip.show();

        break;
        
      case 4:
        Serial.println("Position selection");
        encoderPos = pixelPos;     

        flasher(1, 50);
        
        break;
        
      case 5:
        Serial.println("Operation mode");
        adjustmentState = 0;
        flasher(3, 25);
        break;
        
    }
  } else if (buttonState == 0 && pressed) {
    pressed = false;
    delay(150);
  }

}

void flasher (int iteration, int pause) {
  
  for (int i = 0; i <= iteration; i++) {
    strip.setPixelColor(pixelPos, 75, 75, 75);
    strip.show();
    delay(pause);
    strip.setPixelColor(pixelPos, 0, 0, 0);
    strip.show();  
    delay(pause);
  }

  displayLED();
  
}

void displayLED() {

  for (int i = 0; i <= 8; i++) {
    strip.setPixelColor(i, LED_Array[i][0], LED_Array[i][1], LED_Array[i][2]);
    strip.show();
  }
}

void displayReady() {

  for (int i = 0; i <= 8; i++) {
    strip.setPixelColor(i, 50, 50, 50);
    strip.show();
    delay(50);
    strip.setPixelColor(i, 0, 0, 0);
    strip.show();    
  }
  
}




